package net.codejava.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Id;

@Entity
@Table(name="employees")
public class Employee {
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
	 private Long Id;
		@Column(name = "first_name")
	 private String firstname;
		@Column(name = "middle_name")
	 private String middleName;
		@Column(name = "last_name")
	 private String lastName;
		@Column(name = "email_Id")
	 private String emailId;
	 
	 public Employee() {
		 
	 }
	 
	public Employee(String firstname, String middleName, String lastName, String emailId) {
		super();
		this.firstname = firstname;
		this.middleName = middleName;
		this.lastName = lastName;
		this.emailId = emailId;
	}
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	 
	 
	}


